#!/usr/bin/env python

import cv2
import math
import numpy as np
import sys

class WaterTracker:
    def __init__(self):
        cv2.namedWindow("WaterTracker", cv2.CV_WINDOW_AUTOSIZE)
        self.capture = cv2.VideoCapture(sys.argv[1])
        self.scale_down = 1 # scale down to speed up the process
        self.image_list = ["final", "binary", "canny"]
        self.image_index = 0   # Index into image_list
        self.min_val = 100
        self.max_val = 200

    def run(self):

        while self.capture.isOpened():
            c = cv2.waitKey(20) % 0x100

            # Image manipulation
            _, img = self.capture.read()
            img = cv2.flip(img, 1)
            orig_img = img
            img = cv2.GaussianBlur(img, (5,5), 0)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
            img = cv2.resize(img, (len(img[0]) / self.scale_down, len(orig_img) / self.scale_down))
            lower = np.array([110, 50, 50], np.uint8) # B, G, R
            upper = np.array([130, 255, 255], np.uint8)
            binary = cv2.inRange(img, lower, upper)
            dilation = np.ones((15, 15), "uint8")
            binary = cv2.dilate(binary, dilation)

            # canny edge detection
            edges = cv2.Canny(binary, self.min_val, self.max_val)

            # final image
            contours, hierarchy = cv2.findContours(edges,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
            cv2.drawContours(orig_img,contours,-1,(0,255,0),3)

            # Toggle which image to show (press space)
            if c == 32:
                self.image_index = (self.image_index + 1) % len(self.image_list)

            image_name = self.image_list[self.image_index]

            if image_name == "binary":
                #convert to rgb
                binary = cv2.cvtColor(binary, cv2.COLOR_GRAY2RGB)
                cv2.putText(binary, "Binary", (5, 25), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,0,255), 2)
                cv2.imshow("WaterTracker", binary)
            elif image_name == "canny":
                edges = cv2.cvtColor(edges, cv2.COLOR_GRAY2RGB)
                cv2.putText(edges, "Edges", (5, 25), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,0,255), 2)
                cv2.imshow("WaterTracker", edges)
            elif image_name == "final":
                cv2.putText(orig_img, "Final", (5, 25), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0,0,255), 2)
                cv2.imshow("WaterTracker", orig_img)

            if c == 27:
                cv2.destroyWindow("WaterTracker")
                break

if __name__ == "__main__":
    tracker = WaterTracker()
    tracker.run()
